Journal de Bord:

02/02/2018 par Thomas Naigon:

Aujourd'hui nous avons commencé par récupéré les codes sources sur le gitlab du projet 
KeyProject de Dylan, Alban, Eitcher. Nous avons choisis ce projet car c'était le 
projet qui pour nous semble bien avancé. Ensuite aprés avoir récupéré les codes nous 
avons commencé l'analyse pour prendre connaissances du projet. J'ai donc commencé à 
me penché sur la géolocatisation de la clé. J'ai donc constaté que la géolocalisation 
par GPS n'était pas forcement la bonne idée car dans une salle le module GPS n'est pas 
fonctionnel.


08/02/2018 par Thomas Naigon:

Aujourd'hui j'ai donc analysé l'autre solution de géolocalisation c'est à dire au lieu 
du GPS qui n'est pas efficace on va s'intéressé à la géolocalisation des clés par WiFi 
en partant du principe que le trousseau de clé ne quitte pas le département.
J'ai donc testé le programme de localisation par WiFi sur l'AP mis a disposition 
"LinkitOneWifi". Le programme nous retourne le SSID, l'adresse MAC de l'AP et la force 
du signal. Donc pour localisé les clés ils se sont basé sur l'adresse MAC et la force 
du signal pour déterminer la salle dans laquelle se trouve les clés.
Ensuite j'ai réalisé que pour faciliter la localisation des clés on pourrai ajouter un 
petit signal sonore qui va nous aidé à déterminer la localisation dans la salle. 

 
13/02/2018 :

Aujourd'hui aprés avoir consulté le Git, j'ai décidé de reprendre la où 
mon collègue était resté et donc j'ai repris l'issue "[Identification] 
Saisie de l'utilisateur" et donc je me suis rapidement documenté sur le 
Fingerprint. J'ai suivi les premières étapes de la documentation : 
http://wiki.seeed.cc/Grove-Fingerprint_Sensor/  
Mais aprés l'installation de la première librairie il y a eu une erreur 
de compilation car il manqué 2 éléments : 
- SoftwareSerial.h
- SoftwareSerial.cpp

J'ai donc installé les deux éléments manquant mais cette fois-ci une nouvelle erreur 
apparait : 

Attention: platform.txt du cœur 'MediaTek ARM7 EJ-S (32-bits) Boards' contiens 
recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}"(............)C:\Program Files 
(x86)\Arduino\libraries\AdaFingerprint\Adafruit_Fingerprint.cpp:18:24: fatal error: 
util/delay.h: No such file or directory

 #include <util/delay.h>

                        ^
Suite à cette erreur j'ai décidé de passé à la recherche de solution pour la WiFi.


20/02/2018 :

Aujourd'hui aprés avoir consulté le journal de bord de mon collègue, j'ai donc 
constaté que la solution du lecteur d'empreinte n'était plus d'actualité à cause de 
plein d'erreur de compilation lié au librairie, il avait donc proposé en contrepartie 
une authentification par tag NFC. Je me suis donc documenté sur le module grove NFC :

http://wiki.seeed.cc/Grove-NFC_Tag/

J'ai suivis le tutoriel : - Installer l'application sur le téléphone 
			  - Installer la librairie 
			  - Téléverser le code** 


**code de test que vous pourrais retrouvé ici : 
https://gitlab.com/f.babet/M4207/tree/master/Code/Partie_Identification

Aprés téléversement il n'y a pas eu d'erreur de compilation mais cependant lors du 
test lorsque le téléphone s'approche du tag la led branché sur la pin D2 est censé 
s'allumer ce qui n'est pas le cas.Du coup j'ai esssayé aussi le code de test de mon 
collègue mais je n'avais les librairies nécessaire donc j'ai laissé ça de coté pour le 
moment. 


28/02/2018:

Aujourd'hui je suis repartis sur la partie de la localisation par 
WiFi. J'ai donc récupéré et modifié du code qui va nous permettre de 
récupérer les adresses MAC des AP qui propose une connexion à 
RT-WIFI-Guest. J'ai donc pu tester ce programme qui est maintenant 
fonctionnel. Maintenant pour améliorer notre code il faudrait associé 
les adresses MAC au nom de salle puis affiché le nom de la salle pour 
que l'utilisateur soit informé.
