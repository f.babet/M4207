**Groupe:**
Naïgon Thomas,
Babet Florent

_____________

Code récupéré sur le projet de dylan

_____________
**Caractéristique du système**

_**I)Identification :**_

**NFC Tag:**

**Contexte:**
- Il est important de trouver un système permettant de savoir qui a pris la clé, et à quelle heure. Afin d'avoir une personne responsable de celle-ci. 

**Explication/Procédé**
- Pour la Saisie de l'utilisateur, Une personne souhaitant prendre une clé, devra s'identifier avec son son NFC Tag qui est propre à chaque étudiant, ce qui permettra d'identifié qui a pris la clefs.

**Exemple**
- L'étudiant souhaite prendre une clefs, il se dirige vers la salle d'empreint des clefs, cette personne prends une clés, et doit s'identifier en passant son NFC Tag sur celui de l'objet empreinter, les informations de l'étudiants et donc enregistrer, l'heure etc... en précisant la clef récupéré.

**Installation/ Bug**
- Procédure d'installation, télécharger le dossier zip à cette adresse: https://github.com/Seeed-Studio/PN532, une fois ceci fait, vous récuperez les
dossiers PN532, PN532_SPI, PN532_I2C, PN532_HSU. Ensuite convertissez chacun de ces dossiers en zip. Puis les ajouter à votre blibliothéque Arduino dans "Croquis" "Inclure une blibliothéque" "Ajouter la bliblithéque .ZIP".
Par la suite, vous téléchargez le dossier .ZIP à cette adresse: https://github.com/Seeed-Studio/Grove-NFC-libraries-Part, puis ajouter le dossier ZIP directement grâce à la manip. ci-dessus.

- Bug: La soudure du NFC tag doit être, entre P1 et I2C, malheureusement par défault elle est entre P1 et UART, il faut donc couper P1 UART et changer avec la prémière option précedemment indiqué. 

_**II)Localisation :**_

**Wifi :**

**Contexte**
- Toutes les salles sont équipées D'AP WiFi Unique ayant une adresse MAC, on peut donc normalement savoir quels APs WiFis se trouvent dans quelles salles?

**Explication/Procédé**
- Grâce aux APs Wifi des salles, nous pouvons nous connecter au Réseau du Département **"RT-WIFI"****, la clé sera donc localiser grâce à une localisation WIFI. Ce système permettra de savoir où se trouve la clé dans le département et donc de trouver la salle la plus proche de où se situe la clés. Ensuite il est possible de savoir où se trouve la clé plus précisément grâce à un système de puissance émis, plus la puissance est faible plus la clés se trouve loin ou bien se trouve dans un endroit confiné. Ensuite grâce à ce système si la clé ne détecte pas de Réseau RT-Wifi alors la clés se trouve à l'extérieur du département.

**Voici les adresses MAC des APs de chaque salle du département:**
**RTWIFI:**
- 92:2A:A8:5A:5C:68 Administration
- 92:2A:A8:4A:AD:26 Proj-doc
- 92:2A:A8:4A:AD:2E Res-cab , Info prog
- 92:2A:A8:4A:AF:2E Signaux Systemes
- 92:2A:A8:47:CE:A5 TD1 , TD2
- 92:2A:A8:4A:AC:55 TD3, Info prog

**Eduroam:**
- 00:1D:A2:D8:38:B1 Administration
- 00:07:0E:56:93:80 Proj doc
- 00:1D:A2:69:8E:9A TD2
- 00:1D:A2:D8:3D:E1 Genie inf
- 00:13:1A:96:7D:71 Info prog
- 00:14:6A:F0:41:31 LPRO
- 00:14:6A:F0:42:21 LPRO

**Exemple**
- Nous avons perdu la clefs, la clefs et connecté à un AP-WIFI, dont l’adresse MAC est 92:2A:A8:4A:AD:26, grâce à notre programme, celui-ci indique que c’est l’adresse MAC de la salle proj-doc, donc théoriquement la clefs devrait être à proximité de cette salle 

**GPS :**

**Contexte:**
- La localisation WiFi ne suffit pas, il est intéressant d'obtenir des valeurs précis de l’emplacement de la clé 

**Explication/Procédé**
- Pour la récupération des données GPS, un module GPS sera connecté dans le dispositif de la clé, le GPS sevira à complémenter la localisation par WiFi afin d'obtenir des coordonnées plus au moins précis, les données GPS seront envoyées depuis une antenne dans le dispositifs de la clé et récupérer sur un ordinateur en récupérant les valeurs moyennes des longitude et latitude quand on se place devant une salle:

**Exemple** 
- La clefs est devant la salle Génie Info, on a comme latitude : X et comme longitude : Y


**Bluetooth :**

**Contexte:**
- Après avoir trouvé la salle où se trouve la clé, l'utilisateur devra encore chercher les clés qui sont toujours perdu mais dans moins grand périmètre cette fois-ci. Donc pour pallier ce problème nous allons installé un système sonore actionné à distance

**Explication/Procédé**
- Cette partie consiste à lorsque la localisation Wifi et GPS a été réalisé, que nous avons ses coordonnées, mais nous ne trouvons pas retrouvé la clefs, il sera possible alors de ce connecté à la clé grâce à une link it ONE ayant la bluetooth, grâce à une application mobile, il suffira d’envoyé une commande par exemple « ON » depuis l’application, et ceci déclenchera un son depuis la clefs, Nous aurons donc la position précise grâce aux sons émis. 

**Exemple:**
- Nous somme au courant que la clefs se trouve proche de la salle Proj-Doc, grâce à la wifi 

**Application**
- L'application se trouve à cette adresse: http://www.instructables.com/id/Solving-LinkIt-ONEs-Bluetooth-Connectivity/

_**III)Batterie**_

**Niveau de Batterie**

**Contexte:**
- Tout équipements électroniques à besoin d'être alimenter afin qu'ils puissent fonctionner, il est donc important de trouver un moyen pour prévenir les utilisateurs que le système de localisation de la clé est décharger afin de recharger celui-ci et d'éviter tout risque de perte des clés durant ce laps de temps.

**Explication/Procédé**
- Pour le niveau de la batterie, ils ont affiché le niveau sur un écran LCD (en pourcentage exemple: Batterie: 95%), Ensuite ils ont dit que si on dépasse on certain niveau de la batterie un avertisseur ce Déclenche (Une LED clignote) afin de prévenir que le niveau de batterie commence à être faible. Enfin lorsque la batterie est très faible celui-ci émet un son pour prévenir que le système à besoin d'être recharger (Module Buzzer en réalisant une musique).

**Solution Adoptée**
- **Ecran LCD** (Pour afficher le niveau de la batterie), une **LED** (Clignotante), et un **Module Buzzer** (pour l'avertisseur sonore).