# Exercice 2
---
## Code GPS


    #include <LGPS.h>                                                                                           
                                                                                                              
    gpsSentenceInfoStruct info;                                                                               
    char buff[256];                                                                                           
                                                                                                                      
    static unsigned char getComma(unsigned char num,const char *str)                                          
    {                                                                                                         
      unsigned char i,j = 0;                                                                                  
      int len=strlen(str);                                                                                    
      for(i = 0;i < len;i ++)                                                                                 
      {                                                                                                       
         if(str[i] == ',')                                                                                    
          j++;                                                                                                
         if(j == num)                                                                                         
          return i + 1;                                                                                       
      }                                                                                                       
      return 0;                                                                                               
    }                                                                                                         
                                                                                                              
    static double getDoubleNumber(const char *s)                                                              
    {                                                                                                         
      char buf[10];                                                                                           
      unsigned char i;                                                                                        
      double rev;                                                                                             
                                                                                                              
      i=getComma(1, s);                                                                                       
      i = i - 1;                                                                                              
      strncpy(buf, s, i);                                                                                     
      buf[i] = 0;                                                                                             
      rev=atof(buf);                                                                                          
      return rev;                                                                                              
    }                                                                                                         
                                                                                                              
   static double getIntNumber(const char *s)                                                                  
    {                                                                                                         
      char buf[10];                                                                                           
      unsigned char i;                                                                                        
      double rev;                                                                                                 
                                                                                                              
      i=getComma(1, s);                                                                                       
      i = i - 1;                                                                                              
      strncpy(buf, s, i);                                                                                     
      buf[i] = 0;                                                                                             
      rev=atoi(buf);                                                                                          
      return rev;                                                                                             
    }                                                                                                         
    
    void parseGPGGA(const char* GPGGAstr)
    {
      /* Refer to http://www.gpsinformation.org/dale/nmea.htm#GGA
       * Sample data: $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
       * Where:
       *  GGA          Global Positioning System Fix Data
       *  123519       Fix taken at 12:35:19 UTC
       *  4807.038,N   Latitude 48 deg 07.038' N
       *  01131.000,E  Longitude 11 deg 31.000' E
       *  1            Fix quality: 0 = invalid
       *                            1 = GPS fix (SPS)
       *                            2 = DGPS fix
       *                            3 = PPS fix
       *                            4 = Real Time Kinematic
       *                            5 = Float RTK
       *                            6 = estimated (dead reckoning) (2.3 feature)
       *                            7 = Manual input mode
       *                            8 = Simulation mode
       *  08           Number of satellites being tracked
       *  0.9          Horizontal dilution of position
       *  545.4,M      Altitude, Meters, above mean sea level
       *  46.9,M       Height of geoid (mean sea level) above WGS84
       *                   ellipsoid
       *  (empty field) time in seconds since last DGPS update
       *  (empty field) DGPS station ID number
       *  *47          the checksum data, always begins with *
       */
      double latitude;
      double longitude;
      int tmp, hour, minute, second, num ;
      if(GPGGAstr[0] == '$')
      {
        tmp = getComma(1, GPGGAstr);
        hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
        minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
        second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
        
        sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
        Serial.println(buff);
        
        tmp = getComma(2, GPGGAstr);
        latitude = getDoubleNumber(&GPGGAstr[tmp]);
        tmp = getComma(4, GPGGAstr);
        longitude = getDoubleNumber(&GPGGAstr[tmp]);
        sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
        Serial.println(buff); 
        
        tmp = getComma(7, GPGGAstr);
        num = getIntNumber(&GPGGAstr[tmp]);    
        sprintf(buff, "satellites number = %d", num);
        Serial.println(buff); 
      }
      else
      {
        Serial.println("Not get data"); 
      }
    }
    
    void setup() {
      // put your setup code here, to run once:
      Serial.begin(115200);
      LGPS.powerOn();
      Serial.println("LGPS Power on, and waiting ..."); 
      delay(3000);
    }
    
    void loop() {
      // put your main code here, to run repeatedly:
      Serial.println("LGPS loop"); 
      LGPS.getData(&info);
      Serial.println((char*)info.GPGGA); 
      parseGPGGA((const char*)info.GPGGA);
      delay(2000);
    }                                                                                                         
                                                                                                              
### Résultat

<img src ="Exo2_Resultat.png" length="400" width="200">
---
# Exercice 3
---
## Code Wifi

        #include <LWiFi.h>
        #include <LWiFiServer.h>
        #include <LWiFiClient.h>
        
        #define WIFI_AP "LinkitOneWifi"
        #define WIFI_PASSWORD ""
        #define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
        
        LWiFiServer server(80);
        
        void setup()
        {
          LWiFi.begin();
          Serial.begin(115200);
        
          // keep retrying until connected to AP
          Serial.println("Connecting to AP");
          while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
          {
            delay(1000);
          }
        
          printWifiStatus();
        
          Serial.println("Start Server");
          server.begin();
          Serial.println("Server Started");
        }
        
        int loopCount = 0;
        
        void loop()
        {
          // put your main code here, to run repeatedly:
          delay(500);
          loopCount++;
          LWiFiClient client = server.available();
          if (client)
          {
            Serial.println("new client");
            // an http request ends with a blank line
            boolean currentLineIsBlank = true;
            while (client.connected())
            {
              if (client.available())
              {
                // we basically ignores client request, but wait for HTTP request end
                int c = client.read();
                Serial.print((char)c);
        
                if (c == '\n' && currentLineIsBlank)
                {
                  Serial.println("send response");
                  // send a standard http response header
                  client.println("HTTP/1.1 200 OK");
                  client.println("Content-Type: text/html");
                  client.println("Connection: close");  // the connection will be closed after completion of the response
                  client.println("Refresh: 5");  // refresh the page automatically every 5 sec
                  client.println();
                  client.println("<!DOCTYPE HTML>");
                  client.println("<html>");
                  // output the value of each analog input pin
                  client.print("loop");
                  client.print(" is ");
                  client.print(loopCount);
                  client.println("<br />");
                  client.println("</html>");
                  client.println();
                  break;
                }
                if (c == '\n')
                {
                  // you're starting a new line
                  currentLineIsBlank = true;
                }
                else if (c != '\r')
                {
                  // you've gotten a character on the current line
                  currentLineIsBlank = false;
                }
              }
            }
            // give the web browser time to receive the data
            delay(500);
        
            // close the connection:
            Serial.println("close connection");
            client.stop();
            Serial.println("client disconnected");
          }
        }
        
        void printWifiStatus()
        {
          // print the SSID of the network you're attached to:
          Serial.print("SSID: ");
          Serial.println(LWiFi.SSID());
        
          // print your WiFi shield's IP address:
          IPAddress ip = LWiFi.localIP();
          Serial.print("IP Address: ");
          Serial.println(ip);
        
          Serial.print("subnet mask: ");
          Serial.println(LWiFi.subnetMask());
        
          Serial.print("gateway IP: ");
          Serial.println(LWiFi.gatewayIP());
        
          // print the received signal strength:
          long rssi = LWiFi.RSSI();
          Serial.print("signal strength (RSSI):");
          Serial.print(rssi);
          Serial.println(" dBm");
        }
        
### Résultat

<img src ="Exo3.PNG" length="400" width="200">