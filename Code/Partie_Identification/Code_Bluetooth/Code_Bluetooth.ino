#include <LBT.h>
#include <LBTServer.h>
#define SPP_SVR "StarPla" //On défini le nom du serveur Bluetooth
#define ard_log Serial.printf


int read_size = 0;
void setup() {
  
  Serial.begin(9600);           //on démarre la communication série
  ard_log("start BTS\n");       //on annonce que l'on va lancer le serveur
}


void loop() {
 
bool success = LBTServer.begin((uint8_t*)SPP_SVR); //pour vérifier si le Bluetooth a bien été lancé

  if( !success )
  {
      ard_log("Cannot begin Bluetooth Server successfully\n");
  }
  else
  {
      ard_log("Bluetooth Server begin successfully\n"); //on annonce que le serveur a bien été lancé
  }
 
  // On attend pour voir si on a un client qui se connecte
  bool connected = LBTServer.accept(20); //on met un timeout de 20secondes, le temps qu'a l'utilisateur pour se connecter avant le prochain cycle

 int sent =0;
  if( !connected )
  {
      ard_log("No connection request yet\n");
      // si aucun périphérique n'est connecté
      sent=1; //on met sent à 1 pour dire que l'on ne va pas à recevoir de données
  }
  else
  {
      ard_log("Connected\n"); //si un périphérique est connecté on le notifie
  }


  
  if (!sent)
  {
      char buffer[32] = {0};
      
      memset(buffer, 0, sizeof(buffer));
        delay (10000); //on attends 10 secondes et si l'utilisateur a rien tapé, on arrête la communication
        if(LBTServer.available())
        {
          read_size = LBTServer.readBytes((uint8_t*)buffer, 32); //on va lire ce qu'envoie le téléphone
        }
      
      ard_log("%s[%d]\n",  buffer, read_size);//on affiche ce qui est envoyé par le téléphone sur le moniteur série
  }
  LBTServer.end(); //on arrête le serveur Bluetooth
  delay(10000);   
}
