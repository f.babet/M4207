//Librairie pour la batterie
#include <Wire.h> //Ajout de la bibliothèque i2c pour utiliser le lcd
#include "rgb_lcd.h" //Ajout de la bibliothèque du lcd
#include <LBattery.h> //Ajout de la bibliothèque de la batterie

//Librairie pour la localisation de la clé
#include <LWiFi.h>
#include <LWiFiClient.h>

//#define WIFI_AP_LINK "LinkitOneWifi"            // WiFi AP SSID LINKITONE
#define WIFI_AP "RT-WIFI-Guest"            // WiFi AP SSID RT WIFI GUEST
#define WIFI_PASSWORD "wifirtguest"  // WiFi AP RT mdp
#define WIFI_AUTH LWIFI_WPA           // type d'authentification              
char bssid2[18];             // @MAC de la borne2 WIFI
char *rtwifi[6][2] = {{"92:2A:A8:5A:5C:68","Administration"},
                       {"92:2A:A8:4A:AD:26","Proj-doc"},
                       {"92:2A:A8:4A:AD:2E","Reseaux cablages"},
                       {"92:2A:A8:4A:AF:2E","Signaux Systemes"},
                       {"92:2A:A8:47:CE:A5","TD1"},
                       {"92:2A:A8:4A:AC:55","TD3"}};


//Initialisation pour la partie avec la batterie
  const int ledPin = 3; 
  int niveau;
  int charge;


//Initialisation pour le LCD
    rgb_lcd lcd;  //Constructeur rgb_lcd pour l'objet lcd    

void setup() {

    // partie de la batterie
    lcd.begin(16, 2); //Initialisation du lcd de 16 colonnes et 2 lignes
    Serial.begin(115200);
    pinMode(ledPin, OUTPUT); //Initialisation pour la LED

    //partie de la localisation
     while (!Serial) {
    ;                     // attendre qu'il soit ouvert pour la connection 
  }
    
}

void loop() {

  // partie pour la batterie
  // variables pour le niveau de batterie et lorsqu'il est en charge
  niveau = LBattery.level();
  charge = LBattery.isCharging();
  delay(5000);
  
  //Condition suivant le niveau de batterie
  //Condition si le cable est brancher 
  while (charge == 1) {
    lcd.setCursor(0,0); //On commence à écrire en haut à gauche
    lcd.print("Batterie : "); //On écrit un texte
    lcd.print(niveau); //On écrit un texte
    lcd.print("%");
    digitalWrite(ledPin, HIGH);
    delay(2000);
    digitalWrite(ledPin, LOW);
    delay(2000);
}

  //Condition si on utilise la batterie
  while (charge==0){
    lcd.setCursor(0,0); //On commence à écrire en haut à gauche
    lcd.print("Batterie : "); //On écrit un texte
    lcd.print(niveau); //On écrit un texte
    lcd.print("%");
    digitalWrite(ledPin, HIGH);
    delay(2000);
    digitalWrite(ledPin, LOW);
    delay(2000);

  //Condition s'il reste 66% de batterie
  if ((niveau <= 66) && (niveau > 33)){
    lcd.setRGB(204,255,153);
    digitalWrite(ledPin, HIGH);
    delay(1000);
    digitalWrite(ledPin, LOW);
    delay(1000);
  }

  //Condition s'il reste 33% de batterie
 if (niveau <= 33){
   
    lcd.noDisplay();
    delay(500);
    lcd.display();
    lcd.setRGB(204,0,0);
    delay(500);
    lcd.noDisplay();
    delay(500);
    lcd.display();
    lcd.setRGB(255,255,0);
    delay(500);
    
    digitalWrite(ledPin, HIGH);
    delay(100);
    digitalWrite(ledPin, LOW);
    delay(100);
  }
    
  }

  // partie pour la localisation
    //*** Connexion au wifi ***\\
  LWiFi.begin();        // Initialise le module WiFi
  

  //*** Connexion au RT-WIFI-Guest ****\\
  
  Serial.print("Connexion au WiFi ");
  Serial.println(WIFI_AP);
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  // Statut de connexion
  StatutWifi();

  // On se déconnecte et on attend 5 secondes
  LWiFi.disconnect();
  delay(5000);
  Serial.println();

  Localize(rtwifi);
  Serial.println();
  
}

// partie en dehors du loop pour la partie localisation
// Affichage du statut WiFi
void StatutWifi() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI): ");
  Serial.print(rssi);
  Serial.println(" dBm");

  // print BSSID
  if (strcmp(LWiFi.SSID(),"RT-WIFI-Guest")==0){
    uint8_t BSSID[VM_WLAN_WNDRV_MAC_ADDRESS_LEN] = {0};
    LWiFi.BSSID(BSSID);
    Serial.print("BSSID2 is: ");  
    sprintf(bssid2, "%02X:%02X:%02X:%02X:%02X:%02X", BSSID[0], BSSID[1], BSSID[2], BSSID[3], BSSID[4], BSSID[5]);   
    Serial.println(bssid2);
  }
  else{
    
    Serial.println("Pas de point d'accees RT-WIFI-Guest dans les alentours"); 
  }

}

void Localize(char *rtwifi[6][2]){ 
  int i;
  for (i=0;i<6;i++){
    if (strcmp(bssid2,rtwifi[i][0])==0){
      Serial.println("---------------------------------");
      Serial.print("Je suis entre "); 
      Serial.print(rtwifi[i][1]);
      Serial.print(" ");
    }
  }

  }


