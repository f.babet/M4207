/*
  Web client

 This sketch connects to a website 
 using Wi-Fi functionality on MediaTek LinkIt platform.

 Change the macro WIFI_AP, WIFI_PASSWORD, WIFI_AUTH and SITE_URL accordingly.

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 modified 20 Aug 2014
 by MediaTek Inc.
 */

#include <LWiFi.h>
#include <LWiFiClient.h>

#define WIFI_AP "Tokyo"
#define WIFI_PASSWORD "srea3631"
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
#define SITE_URL "192.168.43.46/GPS/Formulaire.php" //@ Serveur web

LWiFiClient c;

int idt = 50;
int lati = 15;
int longi = 15;
int heure = 15;

String data;

String buildJson() {
  data = "{";
  data+="\n";
  data+="id: ";
  data+=idt;
  data+= ",";
  data+="\n";
  data+="latitude: ";
  data+=lati;
  data+= ",";
  data+="\n";
  data+="longitude: ";
  data+=longi;
  data+= ",";
  data+="\n";
  data+="UTC: ";
  data+=heure;
  data+="\n";
  data+="}";
  Serial.println(data);
  //return data;
}


void setup()
{
  LWiFi.begin();
  Serial.begin(115200);

  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  // keep retrying until connected to website
  Serial.println("Connecting to WebSite");
  while (0 == c.connect(SITE_URL, 80))
  {
    Serial.println("Re-Connecting to WebSite");
    delay(1000);
  }

  // send HTTP request, ends with 2 CR/LF
  buildJson();
  Serial.println("Connected. Sending HTTP GET Request ...."); 
  c.println("POST /Formulaire.php HTTP/1.1");  //modifer /page.php par la page de traitement
  c.println("Host: " SITE_URL);
  c.println("Connection: close");
  c.println("Content-Type: application/json"); 
  c.print("Content-Length: ");
  c.println(data.length());
  c.println();
  c.println(data);  //send the HTTP POST body
   
  // waiting for server response
  Serial.println("waiting HTTP response:");
  while (!c.available())
  {
    delay(100);
  }
}

boolean disconnectedMsg = false;

void loop()
{
  // Make sure we are connected, and dump the response content to Serial
  while (c)
  {
    int v = c.read();
    if (v != -1)
    {
      Serial.print((char)v);
    }
    else
    {
      Serial.println("no more content, disconnect");
      c.stop();
      while (1)
      {
        delay(1);
      }
    }
  }

  if (!disconnectedMsg)
  {
    Serial.println("disconnected by server");
    disconnectedMsg = true;
  }
  delay(500);
}

