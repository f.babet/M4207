#include <LBT.h> 
#include <LBTServer.h>

int ledPin = 13;  //Pin the LED is connected to on the LinkIt ONE
#define BUZZER_PIN               A2            /* sig pin of the buzzer */

int length = 15;         /* the number of notes */
char notes[] = "cddddddddddddd ";
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 300;

String command;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
LBTServer.begin((uint8_t*)"testbabet");
pinMode(ledPin,OUTPUT);
digitalWrite(ledPin,LOW);
/* set buzzer pin as output */
    pinMode(BUZZER_PIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

if(LBTServer.connected())
{
  Serial.println("Connected");  //If bluetooth is connected print "Connected" in serial port
  }
else
{
  Serial.println("Retrying");   //Else retry
  LBTServer.accept(5);
  }

command = LBTServer.readString();
if (command == "On"){
  digitalWrite(ledPin,HIGH);   //If command is "On", switch LED on
  Serial.println("LED is on");
  LBTServer.write("LED is on");
  for(int i = 0; i < length; i++) {
        if(notes[i] == ' ') {
            delay(beats[i] * tempo);
        } else {
            playNote(notes[i], beats[i] * tempo);
        }
        delay(tempo / 2);    /* delay between notes */
    }
  }
else if (command == "Off"){
  digitalWrite(ledPin,LOW);   //If command is "Off", switch LED off
  Serial.println("LED is off");
  LBTServer.write("LED is off");
  }
}


void playTone(int tone, int duration) {
    for (long i = 0; i < duration * 1000L; i += tone * 2) {
        digitalWrite(BUZZER_PIN, HIGH);
        delayMicroseconds(tone);
        digitalWrite(BUZZER_PIN, LOW);
        delayMicroseconds(tone);
    }
}

void playNote(char note, int duration) {
    char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
    int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };

    // play the tone corresponding to the note name
    for (int i = 0; i < 8; i++) {
        if (names[i] == note) {
            playTone(tones[i], duration);
        }
    }
}

