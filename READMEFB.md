**Journal de bord:**

_*02/02/2018 par Florent Babet:*_

- Aujourd'hui nous avons analysé les différents codes sources des anciens RT2, et nous avons récupéré les codes de dylan car c'était les codes les mieux classé, Dans un seconds temps
J'ai réalisé des issues sur les solutions qu'ils ont adoptées afin de mieux appréhender notre projet démarrant depuis le leurs, car il est important de savoir ce qu'ils ont fais, 
et de bien décrire les caractérisques qu'ils ont utilisé, pour qu'a la prochaine séance nous puissons commençer le projet en connaissant ce qu'ils ont fait, les codes qui ne fonctionnent
et qui fonctionnent, et d'établir nos solutions que nous allons faire pour notre projet.

_*08/02/2018*_

- Aujourd'hui j'ai testé le code de la batterie, le buzzer fonctionne bien mais j'ai eu un problème avec le module "Grove-LCD RBG Backlight", le texte ne s'affichait pas, pour régler
le problème il faut régler le "Grove base shield" sur 5V !. Pour l'identification des étudiants nous allons tester la méthode de détecteur d'empreinte digital avec le module 
"Grove - Fingerprint Sensor" lorsque j'ai essayé de lancer un programme, un premier problème est arrivé avec la libraire lors que la compilation il ne détectait pas 
" #include "../SoftwareSerial/SoftwareSerial.h" " j'ai donc télécharger cette blibliothéque: https://github.com/jdollar/espsoftwareserial/, le problème a été réglé. Mais un autre
problème est survenue lors de la compilation "error #include <util/delay.h>", j'ai édité le fichier "Adafruit_Fingerprint.cpp" se trouvant dans la librairie du module fingerprint,
et j'ai mis en commentaire la ligne "#include <util/delay.h>" --> " //#include <util/delay.h>", le problème est donc réglé. Ensuite un autre problème de librairi est survenue:
 
"Attention: platform.txt du cœur 'MediaTek ARM7 EJ-S (32-bits) Boards' contiens recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}" {compiler.ar.flags} "{build.path}/{archive_file}" "{object_file}" dépassé, converti automatiquement en recipe.ar.pattern="{compiler.path}{compiler.ar.cmd}" {compiler.ar.flags} "{archive_file_path}" "{object_file}". La mise a niveau de ce cœur est conseillée.
D:\Arduino\libraries\espsoftwareserial-master\SoftwareSerial.cpp:27:32: fatal error: esp32-hal-gpio.h: No such file or directory
 
 
        #include "esp32-hal-gpio.h"" 
     
     
Je  n'ai pas réussi à régler ce problème pour l'instant !

_*13/02/2018*_

- Aujourd'hui j'ai essayé de debugger le fingerprint, dans un premier temps dans le code exemple enroll j'ai modifié la ligne: #include <SoftwareSerial.h> par: #include <D:\Arduino\hardware\arduino\avr\libraries\SoftwareSerial\src\SoftwareSerial.h>
Ensuite je suis allé dans le fichier "Adafruit_Fingerprint.h" et j'ai modifié la ligne: #include <SoftwareSerial.h> par: #include <D:\Arduino\hardware\arduino\avr\libraries\SoftwareSerial\src\SoftwareSerial.h>
Ensuite je suis allé dans le fichier "Adafruit_Fingerprint.cpp" et j'ai modifié la ligne: #include <SoftwareSerial.h> par: <D:\Arduino\hardware\arduino\avr\libraries\SoftwareSerial\src\SoftwareSerial.h>
Enfin toujours dans le même fichier j'ai mis en commentaire: #include <util/delay.h> --> //#include <util/delay.h
La compilation Marche.
 
Un nouveau problème est survenue, le code nest pas adapté à du linkitone, il faut donc réalisé une portabilité de Arduino vers LinkIt One en modifiant le code, Chose que je ne sais pas faire
J'ai donc abandonné l'idée du finger print pour passer sur un module "Grove NFC" 

_*23/02/2018*_

- Aujourd'hui j'ai opté pour la solution du Grove NFC, la documentation du grove nfc se trouve à cette adresse: "http://wiki.seeed.cc/Grove-NFC/" et non l'adresse internet de Thomas car il s'était
malheureusement il s'était trompé d'adresse. Procédure d'installation, télécharger le dossier zip à cette adresse: https://github.com/Seeed-Studio/PN532, une fois ceci fait, vous récuperez les
dossiers PN532, PN532_SPI, PN532_I2C, PN532_HSU. Ensuite convertissez chacun de ces dossiers en zip. Puis les ajouter à votre blibliothéque Arduino dans "Croquis" "Inclure une blibliothéque" "Ajouter la bliblithéque .ZIP".
Par la suite, vous téléchargez le dossier .ZIP à cette adresse: https://github.com/Seeed-Studio/Grove-NFC-libraries-Part, puis ajouter le dossier ZIP directement grâce à la manip. ci-dessus.
Enfin il faut souder sur sur la partie arriére du Grove NFC, "I2C" avec "P1" malheureusement par manque de matériel, il est impossible de réaliser cette opération lors de cette séance, le professeur 
m'a indiqué qu'il aura réalisé cette soudure pour la prochaine séance. Du coup, je vais travailler sur une autre partie du programme. 
J'ai récupéré les addresses mac des ap en fonction des salles, elles sont dans l'issue Localisation WiFi ! 

_*28/02/2018*_

- Aujourd'hui j'ai tenté de faire marché le Grove NFC malheureusement, celui ci ne fonctionne pas car la soudure necessaire à son bon fonctionnement ne fonctionne pas. J'ai donc
arreté cette partie identification, et je passé à la localisation, j'ai réussi mon objectif! Fonctionnement je fais sonné un buzzer se trouvant sur la clès, à distance grâce à une
application bluetooth android se trouvant sur ce site: "http://www.instructables.com/id/Solving-LinkIt-ONEs-Bluetooth-Connectivity/" l'application me sert juste à une faciliter graphique.
J'ai mis le code: "L_buzzer.ino" dans le gitlab, ce code permet de créer le serveur bluetooth dans la linktit one et permet de faire sonner le buzzer !